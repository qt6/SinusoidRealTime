#include "mainwindow.h"
#include "ui_mainwindow.h"



#include<cmath>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //drawGraph();

}

MainWindow::~MainWindow()
{
   killTimer(timerId);
    delete ui;
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
 drawGraph(arg1);

}


void MainWindow::timerEvent(QTimerEvent *evt){
    delta+=0.1;
    drawGraph(ui->spinBox->value());

}


void MainWindow::drawGraph(int param){


    int size=180;
    //timer

qDebug()<<scaleSize;
    QVector <double> x(size),y(size);



    for(int i=0;i<size;++i){
        x[i]=(i*3*M_PI/180+delta)*scaleWidth;
        y[i]=qSin(x[i]*param);
    }




    ui->widget->addGraph();
    ui->widget->graph(0)->setData(x,y);
    ui->widget->xAxis->setRange((0+delta)*scaleWidth,(3*M_PI+delta)*scaleWidth);
    ui->widget->xAxis->setTicker(QSharedPointer<QCPAxisTickerPi>(new QCPAxisTickerPi));
    ui->widget->yAxis->setRange(-scaleSize,scaleSize);
    ui->widget->replot();


}



void MainWindow::keyPressEvent(QKeyEvent *event ){
int key=event->key();

if (key==Qt::Key_Control)

    isPressed=true;

}

void MainWindow::keyReleaseEvent(QKeyEvent *event ){
  int key=event->key();
    if (key==Qt::Key_Control)

        isPressed=false;

    }








void MainWindow::wheelEvent ( QWheelEvent * event )
{
double valueWheel=event->delta();

if(valueWheel>0){
    if(isPressed)
        scaleWidth*=1.1;
    else
        scaleSize*=1.1;
}
else if(valueWheel<0){
    if(isPressed)
        scaleWidth/=1.1;
    else
        scaleSize/=1.1;
}


}
